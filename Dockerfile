#Dev environment for CSC 430

FROM ubuntu:15.04
MAINTAINER Phillip Wright <pwright4@murraystate.edu>
RUN apt-get update
RUN apt-get install -y openjdk-8-jdk maven git
RUN mkdir /csc430
VOLUME /csc430
