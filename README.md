# Development Environment For CSC-430

This repo contains files needed for (re)creating the dev environment for CSC-430 projects. You should fork this repo and maintain your own copy which you can use to customize the files to your specific needs.

## Docker

The included Dockerfile will allow you to create images and containers for the course.

To use the Dockerfile, you will first need to install Docker on your machine (please consult the official Docker documentation to learn how to do this on your specific system). Once Docker has been installed, you may use the Dockerfile as follows:

To create an image for the development environment, navigate to the directory containing the Docker file and execute the following:

    docker build -t csc430/devenv:v1 . 

You can confirm that the image was created by executing:

    docker images

To create a container based on the image you have created, you may execute:

    docker run -t -i -v *ADIRECTORYONYOURMACHINE*:/csc430 csc430/devenv:v1 /bin/bash

Note: You must replace *ADIRECTORYONYOURMACHINE* with an actual path to a directory on your host machine. The host directory you supply will be mounted to the directory /csc430 within your container and can be used to share data between your host and container.
